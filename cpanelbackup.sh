#!/bin/bash

source backup_server_login_details.sh

yum install epel* -y
yum install sshpass -y

AbsolutePath(){
    abpath="";
    abpath=$(whereis $1 | awk '{ print $2 }' | tr -d "\n")
    echo "$abpath";
}

sshpass_path=$(AbsolutePath sshpass);
ssh_path=$(AbsolutePath ssh);
scp_path=$(AbsolutePath scp);

if [ -z $sshpass_path ]
then
        echo "sshpass_path is file missing";
        exit;
fi

ConChk=$($sshpass_path -p $RootPassword $ssh_path -p $Port -o StrictHostKeyChecking=no -o ConnectTimeout=5 root@$getIP echo connected 2>/dev/null)
if [ "$ConChk" != "connected" ]
then
        echo "SSH access is failed";
        exit;
else
        echo "SSH connected";
fi

if [ ! -z $BackupPeriod ]
then
	$sshpass_path -p $rootpassword $ssh_path -o StrictHostKeyChecking=no root@$getIP 'find '$BackupDir' -type f -name "*.tar.gz" -mtime +'$BackupPeriod' -exec rm -rf {} \;' 2>/dev/null
fi

ls /var/cpanel/users/ > /root/users.txt

while read user
do
    echo $user

    if [ ! -d /home/$user ]
    then
        continue;
    fi
    /scripts/pkgacct $user /home/
    $sshpass_path -p $RootPassword $scp_path -P $Port -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null /home/cpmove-$user.tar.gz root@$getIP:$BackupDir/
    rm -f /home/cpmove-$user.tar.gz

done</root/users.txt